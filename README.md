# Bledar Ramo > Test

## Sate Managment
This is a tiny project and we may not even need state managment but lets consider we want to extend later on so 
Im using ngrx for state managment plus facade pattern so we simplify the NgRx interactions in one place so the Component interact with the Facade in one place

## Structure
We dont have any guards , interceptors, localization etc  so there is not code folder, but I have feature folder for the lazy loaded modules/pages 
and each component is splitted in smart and dump components .Sahred folder has functionalities , components , services etc that we would 
need to-enduse in more than one place


## Search
For country search component I have use debounceTime(400) and distinctUntilChanged()) so it doesnt call/consume all the the time the api and
for each key stroke .


## Local storage
In order to improve  even more app performance and at same time reduce api call consumptions we use localstorage to save  countries list 
and update this country list every 30 days .Of course this can be used for data that we know are not going to change every couple of minutues
Also we store user preferencec there like theme mode for example, so when user comes back his favorite mode is already there


## Styling Framework + custom styling
I have material design for this project , and theme splitting also custom styling to match the design
if you try the app from smaller devices like table and mobile is fully responsive  

## Testing
I could write unit tests for components , store and service but is out of the scope of this test project
If is required please feel free to let me know, and I will write around 80% coverage
