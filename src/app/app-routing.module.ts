import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { MetaTagsSearchEngineService } from './shared/meta-tags-search-engine.service';

const META_DESCRIPTION =
  'An application to view details on countries, maintain a list of visited countries and to maintain a wish list of countries you would like to visit.';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/countries',
    pathMatch: 'full',
    data: {
      title: MetaTagsSearchEngineService.appTitle,
      description: META_DESCRIPTION,
    },
  },
  {
    path: 'countries',
    loadChildren: () =>
      import('./features/countries/countries.module').then((m) => m.CountriesModule),
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {
      title: `${MetaTagsSearchEngineService.appTitle} | Page Not Found`,
      description: META_DESCRIPTION,
    },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
