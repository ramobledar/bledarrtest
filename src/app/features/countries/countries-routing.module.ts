import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MetaTagsSearchEngineService } from '../../shared/meta-tags-search-engine.service';
import { CountriesComponent } from './countries.component';
import { CountryDetailComponent } from './country-detail/country-detail.component';

const routes: Routes = [
  {
    path: '',
    component: CountriesComponent,
    data: {
      title: `${MetaTagsSearchEngineService.appTitle} | Countries`,
      description: 'A list of countries in the world.',
    },
  },
  {
    path: ':name',
    component: CountryDetailComponent,
    data: {
      title: `${MetaTagsSearchEngineService.appTitle} | {{ country }}`,
      description: `Detailed information about {{ country }}.`,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CountriesRoutingModule {}
