import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountriesRoutingModule } from './countries-routing.module';
import { CountriesComponent } from './countries.component';
import { CountryDetailComponent } from './country-detail/country-detail.component';
import { SharedModule } from '../../shared/shared.module';
import { CountryFilterComponent } from './country-filter/country-filter.component';
import { MaterialModule } from '../../shared/material/material.module';
import { ReactiveFormsModule} from "@angular/forms";
import {CountryGridComponent} from "./country-grid/country-grid.component";
import {HeaderComponent} from "../../shared/header/header.component";
import {MatToolbarModule} from "@angular/material/toolbar";

@NgModule({
  declarations: [
    CountriesComponent,
    CountryDetailComponent,
    CountryFilterComponent,
    CountryGridComponent,
    HeaderComponent,
  ],
  imports: [CommonModule, CountriesRoutingModule, MaterialModule, SharedModule, ReactiveFormsModule, MatToolbarModule],
  exports: [
    HeaderComponent
  ]
})
export class CountriesModule {}
