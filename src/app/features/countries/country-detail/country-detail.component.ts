import {Component, OnInit} from '@angular/core';
import { Location } from '@angular/common'
import {ActivatedRoute, ParamMap} from '@angular/router';
import {CountryDetailFacadeService} from 'src/app/store/facades/country-detail-facade.service';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.scss'],
})

export class CountryDetailComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    public countryDetailFacade: CountryDetailFacadeService,
    private location: Location,
  ) {
  }

  ngOnInit() {
    this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          const path = params.get('name')?.replace(/-/g, ' ');
          this.countryDetailFacade.select(path as string);
          return this.countryDetailFacade.country$
        })
      )
      .subscribe();
  }

  back(): void {
    this.location.back()
  }


}
