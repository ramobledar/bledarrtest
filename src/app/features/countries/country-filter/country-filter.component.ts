import {Component, EventEmitter, Output } from '@angular/core';
import {debounceTime, distinctUntilChanged, startWith} from "rxjs/operators";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-country-filter',
  templateUrl: './country-filter-component.html',
  styleUrls: ['./country-filter.component.scss'],
})
export class CountryFilterComponent {

  @Output() countrySearchChange = new EventEmitter<string>();
  @Output() regionChange = new EventEmitter<string>();

  defaultRegion = 'all-regions';
  regions = ['africa', 'americas', 'antarctic', 'asia', 'europe', 'oceania'];
  searchControl = new FormControl();
  private debounce: number = 200;

  constructor() {
  }

  ngOnInit() {
    this.searchControl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(this.debounce),
        distinctUntilChanged())
      .subscribe((query:string) => {
        this.countrySearchChange.emit(query)
      });
  }
}
