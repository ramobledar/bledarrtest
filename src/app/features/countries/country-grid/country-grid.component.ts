import { Component, Input } from '@angular/core';
import CountrySummaryViewModel from '../shared/models/country-summary-view.model';

@Component({
  selector: 'app-country-grid',
  templateUrl: './country-grid.component.html',
  styleUrls: ['./country-grid.component.scss'],
})
export class CountryGridComponent {
  @Input() countries: CountrySummaryViewModel[] = [];
  constructor() {}
}
