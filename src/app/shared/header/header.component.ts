import { Component, OnInit } from '@angular/core';
import * as UserSettingsActions from '../../store/actions/user-settings.actions';
import { HeaderFacadeService } from 'src/app/store/facades/header-facade.service';
import {Theme} from "../theme/local-storage-theme.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {

  constructor(public headerFacade: HeaderFacadeService) {}

  changeTheme(theme: Theme) {
    this.headerFacade.dispatch(UserSettingsActions.changeTheme({ theme }));
  }
}
