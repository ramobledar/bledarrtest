import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})

export class LocalStorageService {
  private constructor() {}

  static setItemWithExpiry(key: string, value: string, ttl: number) {
    const now = new Date();
    const item = {
      value,
      expiry: now.getTime() + ttl,
    };
    localStorage.setItem(key, JSON.stringify(item));
  }

  static getOrRemoveExpiredItem(key: string) {
    const itemStr = localStorage.getItem(key);
    if (!itemStr) {
      return undefined;
    }
    const item: { value: string; expiry: number } = JSON.parse(itemStr);
    const now = new Date();
    if (now.getTime() > item.expiry) {
      localStorage.removeItem(key);
      return undefined;
    }
    return JSON.parse(item.value);
  }
}
