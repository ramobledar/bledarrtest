import {Theme} from "../theme/local-storage-theme.service";

export default interface HeaderViewModel {
  theme: Theme;
}
