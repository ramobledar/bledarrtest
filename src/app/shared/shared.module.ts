import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NotFoundComponent} from './not-found/not-found.component';
import {MaterialModule} from './material/material.module';
import {MatToolbarModule} from "@angular/material/toolbar";

@NgModule({
  declarations: [
    NotFoundComponent,
  ],
  imports: [CommonModule, RouterModule, MaterialModule, MatToolbarModule],
  exports: [],
})
export class SharedModule {
}
