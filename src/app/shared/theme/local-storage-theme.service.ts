import { Injectable } from '@angular/core';

export type Theme = 'theme-light' | 'theme-dark';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageThemeService {
  constructor() { }

  public get Theme() {
    return (localStorage.getItem('theme') as Theme) || 'theme-light';
  }

  public set Theme(theme: Theme) {
    localStorage.setItem('theme', theme);
  }
}
