import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import CountryDetailViewModel from 'src/app/features/countries/shared/models/country-detail-view.model';
import * as CountryDetailSelectors from '../selectors/country-detail-selectors';

@Injectable({
  providedIn: 'root',
})
export class CountryDetailFacadeService {
  country$: Observable<CountryDetailViewModel | undefined> = of(undefined);
  constructor(private store: Store) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
  }

  select(countryName: string) {
    this.country$ = this.store.select(
      CountryDetailSelectors.selectCountryDetailViewModel(countryName)
    );
  }
}
