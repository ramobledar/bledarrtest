import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import HeaderViewModel from 'src/app/shared/models/header-view.model';
import * as HeaderSelectors from '../selectors/header.selectors';

@Injectable({
  providedIn: 'root',
})
export class HeaderFacadeService {
  headerInfo: Observable<HeaderViewModel>;

  constructor(private store: Store) {
    this.headerInfo = this.store.select(HeaderSelectors.selectHeaderViewModel);
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}
