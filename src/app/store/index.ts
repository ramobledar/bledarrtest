import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromCountry from './reducers/country.reducer';
import * as fromUserSettings from './reducers/user-settings.reducer';

export interface AppState {
  [fromCountry.countriesFeatureKey]: fromCountry.State;
  [fromUserSettings.userSettingsFeatureKey]: fromUserSettings.State;
}

export const reducers: ActionReducerMap<AppState> = {
  [fromCountry.countriesFeatureKey]: fromCountry.reducer,
  [fromUserSettings.userSettingsFeatureKey]: fromUserSettings.reducer,
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production
  ? []
  : [];
