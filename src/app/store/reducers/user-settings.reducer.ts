import { createReducer, on } from '@ngrx/store';
import * as UserSettingsActions from '../../store/actions/user-settings.actions';
import {Theme} from "../../shared/theme/local-storage-theme.service";

export const userSettingsFeatureKey = 'userSettings';

export interface State {
  theme: Theme;
}

export const initialState: State = {
  theme: 'theme-light',
};

export const reducer = createReducer(
  initialState,
  on(
    UserSettingsActions.changeThemeSuccess,
    UserSettingsActions.loadUserSettingsSuccess,
    (state, { theme }): State => {
      return {
        ...state,
        theme,
      };
    }
  )
);
