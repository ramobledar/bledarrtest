import { createSelector } from '@ngrx/store';
import CountrySummaryViewModel from 'src/app/features/countries/shared/models/country-summary-view.model';
import * as CountrySelectors from './country.selectors';

// View Models
export const selectCountrySummaryViewModels = createSelector(
  CountrySelectors.selectAllCountries,
  (countries) => {
    const countrySummaries: CountrySummaryViewModel[] = [];
    countries.map((country) => {
      const summary: CountrySummaryViewModel = {
        name: country.name.common,
        flagUrl: country.flags.svg,
        population: country.population,
        region: country.region,
        capital: country.capital ? country.capital[0] : '',
      };
      countrySummaries.push(summary);
    });
    return countrySummaries;
  }
);

export const selectCountrySummaryViewModelsByRegion = (region: string) =>
  createSelector(selectCountrySummaryViewModels, (countries) =>
    region !== 'all-regions'
      ? countries.filter((country) => country.region.toLowerCase() === region)
      : countries
  );

export const selectCountrySummaryViewModelsByPartialName = (
  partialName: string
) =>
  createSelector(
    selectCountrySummaryViewModels,
    CountrySelectors.selectCommonToOfficialName,
    (countrySummaryViewModels, commonToOfficialName) =>
      countrySummaryViewModels.filter(
        (country) =>
          country.name.toLowerCase().includes(partialName.toLowerCase()) ||
          commonToOfficialName[country.name]
            .toLowerCase()
            .includes(partialName.toLowerCase())
      )
  );

// export const selectCountrySummaryViewModelsByNames = createSelector(
//   selectCountrySummaryViewModels,
//   (countrySummaryViewModels) => {
//     return countrySummaryViewModels.filter((country) =>
//       wishList.includes(country.name)
//     );
//   }
// );
