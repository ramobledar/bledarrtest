import { createSelector } from '@ngrx/store';
import HeaderViewModel from 'src/app/shared/models/header-view.model';
import * as UserSettingsSelectors from './user-settings.selectors';

export const selectHeaderViewModel = createSelector(
  UserSettingsSelectors.selectTheme,
  (theme) => {
    const headerViewModel: HeaderViewModel = {
      theme,
    };
    return headerViewModel;
  }
);
